class Board

attr_accessor :grid

def self.default_grid
  Array.new(10) { Array.new(10) }
end

def initialize(grid = Board.default_grid)
  @grid = grid
end

def [](pos)
  row, col = pos
  @grid[row][col]
end

def []=(pos, mark)
  row, col = pos
  @grid[row][col] = mark
end

def count
  count = 0
  grid.each do |subarray|
    count += subarray.count(:s)
  end
  count
end

def empty?(array = nil)
  if array
    return false unless self[array].nil?
  elsif array == nil
    return false if self.count != 0
  end
  true
end

def full?
  grid.flatten.none?(&:nil?)
end

def won?
  grid.flatten.none? { |val| val == :s }
end

def display
  header = (0..9).to_a.join("  ")
  puts "  #{header}"
  grid.each_with_index { |row, i| display_row(row, i) }
end

  def display_row(row, i)
    chars = row.map { |el| DISPLAY_HASH[el] }.join("  ")
    puts "#{i} #{chars}"
  end

def place_random_ship
  raise "error" if self.full?
  self[random_generator] = :s if empty?(random_generator)
end

def random_generator
  a_length = @grid.length
  s_length = @grid[0].length
  [rand(a_length), rand(s_length)]
end

end
